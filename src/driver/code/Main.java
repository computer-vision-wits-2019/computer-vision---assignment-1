//TODO get the homography function working
// Stitch some images together
// find keypoints from overlapping blurs and octaves

package driver.code;

import SIFT.*;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.*;
import utilities.Image_Helper;
import utilities.LinearAlg;
import utilities.OCV;
import utilities.TupleExtrema;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;

import static utilities.Image_Helper.*;
import static utilities.Image_Helper.displayImage;

public class Main {

    public static void main(String[] args) throws IOException {
	System.out.println("Welcome =)");

        for (int testNum = 1; testNum <= 3; testNum++) {
            System.out.println("");
            System.out.println("Test number " + testNum + " underway...");

            String fileNameDst = "C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\Images\\test"+testNum+",1.jpg";
            String fileNameSrc = "C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\Images\\test"+testNum+",2.jpg";

            //Hyperparameters
            int numOctave = 6; //TODO still only displaying numOctave -1 octaves ??
            int numBlur = 7;

            //Instantiate SOURCE Scale_Space object
            Scale_Space ssSrc = new Scale_Space(fileNameSrc, numOctave, numBlur);
            int widthSrc = ssSrc.octArr[0].width;
            int heightSrc = ssSrc.octArr[0].height;

            //Instantiate DESTINATION Scale_Space object
            Scale_Space ssDst = new Scale_Space(fileNameDst, numOctave, numBlur);
            int widthDst = ssDst.octArr[0].width;
            int heightDst = ssDst.octArr[0].height;

            //DoG & Extrema
            TupleExtrema[][] allExtArrSrc = Extrema.calcAllExtrema(ssSrc, numOctave, numBlur);
            TupleExtrema[][] allExtArrDst = Extrema.calcAllExtrema(ssDst, numOctave, numBlur);
            System.out.println("Scale-space, DoGs and Extrema created...");

            //Save and Display outputs //TODO make into Image_Helper.method //////////////////////
            System.out.println("Displaying and saving outputs...");
            //SRC
            //Scale-Space
            for (int oct = 0; oct < ssSrc.octArr.length - 1; oct++) {
                for (int blur = 0; blur < ssSrc.octArr[oct].data.length - 1; blur++) {
                    BufferedImage img = arrayToImage(ssSrc.octArr[oct].data[blur]);
                    displayImage(img);
                    FileOutputStream out = new FileOutputStream("C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\Results\\Test"+testNum+"\\SRC_Octave"+oct+"_Blur"+blur+".png");
                    ImageIO.write(img, "png", out);
                }
                //DoGs
                int[][][] DoGArrSrc = DoG.calc(ssSrc.octArr[oct]);
                for (int DoG = 0; DoG < DoGArrSrc.length-1; DoG++) {
                    BufferedImage img = arrayToImage(DoGArrSrc[DoG]);
                    displayImage(img);
                    FileOutputStream out = new FileOutputStream("C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\Results\\Test"+testNum+"\\SRC_Octave"+oct+"_DoG"+DoG+".png");
                    ImageIO.write(img, "png", out);
                }
                //Extrema
                displayAndSaveImageWithExtrema(fileNameSrc, allExtArrSrc[0],testNum,"SRC");
            }
            //DST
            //SRC
            //Scale-Space
            for (int oct = 0; oct < ssDst.octArr.length - 1; oct++) {
                for (int blur = 0; blur < ssDst.octArr[oct].data.length - 1; blur++) {
                    BufferedImage img = arrayToImage(ssDst.octArr[oct].data[blur]);
                    displayImage(img);
                    FileOutputStream out = new FileOutputStream("C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\Results\\Test"+testNum+"\\DST_Octave"+oct+"_Blur"+blur+".png");
                    ImageIO.write(img, "png", out);
                }
                //DoGs
                int[][][] DoGArrSrc = DoG.calc(ssDst.octArr[oct]);
                for (int DoG = 0; DoG < DoGArrSrc.length-1; DoG++) {
                    BufferedImage img = arrayToImage(DoGArrSrc[DoG]);
                    displayImage(img);
                    FileOutputStream out = new FileOutputStream("C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\Results\\Test"+testNum+"\\DST_Octave"+oct+"_DoG"+DoG+".png");
                    ImageIO.write(img, "png", out);
                }
                //Extrema
                displayAndSaveImageWithExtrema(fileNameDst, allExtArrDst[0],testNum,"DST");
            }
            //////////////////////////////////////////


            //TODO add up KeyPoint scores across octaves and scales/blurs
            //TODO match points KNN from descriptors 1)contrast(done) 2)KeyPoint score
            //get Homography using Octave 1 extrema - TEMP until KeyPoint scores are implemented
            Point[][] matchedPoints = Match.matchAll(allExtArrDst[0], allExtArrSrc[0]);

            //the number of KeyPoints to be matched and tested
            int numPoints = matchedPoints[0].length;
            System.out.println("numPoints of matched: " + numPoints);

            MatOfPoint2f points2fSrc = new MatOfPoint2f(matchedPoints[1]); //SRC //https://stackoverflow.com/questions/31746044/mat-to-matofpoint2f/31747437
            MatOfPoint2f points2fDst = new MatOfPoint2f(matchedPoints[0]); //DST
            ////TODO make into a method

            //Use Key points to get Homography
            Mat homographyMat = Calib3d.findHomography(points2fSrc, points2fDst, Calib3d.RANSAC, 10); //FIXME returning integer values why??  //TODO takes an input of 'matching points' using knn matches to get points from KeyPoint descriptors

            //Display Homography Matrix
            double[][] arrH = OCV.matrixToArrayDouble(homographyMat);
            double[][] arrHT = new double[3][3];
            System.out.println("");
            System.out.println("Homography: " + arrH.length + " x " + arrH[0].length + " transformation matrix");
            for (int i = 0; i < arrH[0].length; i++) {
                System.out.println(" ");
                for (int j = 0; j < arrH.length; j++) {
                    System.out.print(arrH[j][i] + " ");
                    arrHT[i][j] = arrH[j][i];
                }
            }

            //Apply Homograhpy to test
            int[][] arrImgSrc = imageToArray(getImage(fileNameSrc));
            int[][] arrImgDst = imageToArray(getImage(fileNameDst));

            //Transform and stitch
            int[][] imgArrOut = LinearAlg.affineTransformAndStitch(arrHT, arrImgSrc, arrImgDst);
            BufferedImage img = Image_Helper.arrayToImage(imgArrOut);
            displayImage(img);
            FileOutputStream out = new FileOutputStream("C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\Results\\Test"+testNum+"\\STITCHED_IMAGE.png");
            ImageIO.write(img, "png", out);

            System.out.println("");
            System.out.println("____________________________ ");
        }
    }
    /////////// END OF MAIN ///////////
}

