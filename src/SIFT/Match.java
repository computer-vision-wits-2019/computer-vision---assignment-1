package SIFT;

import org.opencv.core.Point;
import utilities.TupleExtrema;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Match {

    public static Point[][] matchAll (TupleExtrema[] destExtArr, TupleExtrema[] srcExtArr) {
        int length  = srcExtArr.length; //sweep through all candidates //TODO also too long
        int count = 0;
        List<Point> matchListDest = new ArrayList<Point>();
        List<Point> matchListSrc = new ArrayList<Point>();

        for (int i = 0; i < length-1; i++) { //need to change this back to length
            Match.matchOne(destExtArr,srcExtArr[i]);
            if (srcExtArr[i].match.x != 0 && srcExtArr[i].match.y != 0) {
                matchListSrc.add(new Point(srcExtArr[i].x,srcExtArr[i].y));
                matchListDest.add(new Point(srcExtArr[i].match.x,srcExtArr[i].match.y));
                count++;
            }
        }

        //convert Point list to Point Array
        Point[][] matchArr = new Point[2][count];
        for (int i = 0; i < count; i++) {
            matchArr[0][i] = matchListDest.get(i);
            matchArr[1][i] = matchListSrc.get(i);
        }
        return  matchArr;
    }


    public static void matchOne (TupleExtrema[] destExtArr ,TupleExtrema srcExt ){
        int length = destExtArr.length; //TODO way too long - i.e. need to change this to a list to get proper length of extrema arr
        List<TupleExtrema> extList = new ArrayList<TupleExtrema>();
        int count = 0;

        for (int i = 0; i < length-1; i++) {
            int srcExtCL = srcExt.contrastLevel;
            int dist = Math.abs(srcExtCL - destExtArr[i].contrastLevel);
            if (dist == 0){ // if the candidate and the destination point are the same //or similar contrast
                extList.add(destExtArr[i]);
                count ++;
            }
        }
        if (count != 0 && count !=1 ) {
            //Randomly choose match from list
            int rand = getRandomNumberInRange(0, count-1);
            TupleExtrema match = extList.get(rand);
            //convert to OpenCV point in Extrema object
            srcExt.match = new Point(match.x, match.y);
        }else if (count == 1){
            TupleExtrema match = extList.get(0);
            //convert to OpenCV point in Extrema object
            srcExt.match = new Point(match.x, match.y);
        }
    }


    private static int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }


}
