package SIFT;

import utilities.TupleExtrema;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Extrema {

    public static boolean isMax (int x, int y, int[][] bottom, int[][] mid, int[][] top)
    { //is the pixel in the centre of the cube between the DoGs the max pixel ?
      //get value of pixel and compare it to all surrounding pixels one level at a time

        //lengths are 1 indexed - arrays aer 0 indexed

        //reference pixel
        int refPix = mid[x][y];
        int width = mid.length;
        int height = mid[0].length;
        int threshold = 6 ; //rejecting points of low contrast

        //if at any point the value of refPix is less we can stop, as this means it is not max i.e. stopping short of the en
        if (x==0 || y==0 || x==width-1 || y==height-1 || mid[x][y] < threshold){return false ;} //EDGE CASES and THRESHOLD //TODO investigate whether or not dynamic thresholding accorinf to octave imporves performance

        else {
            for (int i = 0; i < 3; i++) {
                if (refPix < bottom[x - 1][y - 1 + i] || refPix < bottom[x][y - 1 + i] || refPix < bottom[x + 1][y - 1 + i]) {
                    return false;
                }
                if (refPix < mid[x - 1][y - 1 + i] || refPix < mid[x][y - 1 + i] || refPix < mid[x + 1][y - 1 + i]) {
                    return false;
                }
                if (refPix < top[x - 1][y - 1 + i] || refPix < top[x][y - 1 + i] || refPix < top[x + 1][y - 1 + i]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static TupleExtrema [][] calcAllExtrema (Scale_Space ss, int numOctave, int numBlur) throws IOException {

        TupleExtrema allExtArr[][] = new TupleExtrema[numOctave][(ss.octArr[0].width * ss.octArr[0].height) / (3 * 3 * 3)];

        for (int oct = 0; oct < ss.numOctave - 1; oct++) {
            int width = ss.octArr[oct].width;
            int height = ss.octArr[oct].height;
            //DoG
            int[][][] DoGArr = DoG.calc(ss.octArr[oct]);
            List<TupleExtrema> extList = new ArrayList<TupleExtrema>();
//            TupleExtrema[] extArr = new TupleExtrema[(height * width * numBlur)]; //what is the theortical max number of extrema ??
            int count = 0;

            for (int blur = 0; blur < ss.numBlur - 1; blur++) {
                //Extrema
                if (blur > 1 && blur < DoGArr.length) {
                    int stride = 1; //dynamically decrease stride as octave level increases?
                    for (int x = 0; x < width; x = x + stride) {
                        for (int y = 0; y < height; y = y + stride) {
                            if (Extrema.isMax(x, y, DoGArr[blur - 1], DoGArr[blur], DoGArr[blur + 1])) {
                                extList.add(new TupleExtrema(blur,x,y, DoGArr[blur][x][y]));
//                                extArr[count] = new TupleExtrema(blur, x, y, DoGArr[blur][x][y]); // TODO make this array extArr[blur][count] so that using [blur-1] we can try find overlapping extrema for key points...
//                                if(extArr[count].x ==  ) //
                                count++;
                            }
                        }
                    }
                }
            }
            TupleExtrema [] extArr = new TupleExtrema[count];
            for (int i = 0; i <count; i++) {
                extArr[i] = extList.get(i);
            }
            allExtArr[oct] = extArr;
        }
        return allExtArr;
    }

}
