package SIFT;

import org.opencv.core.Mat;
import java.awt.image.BufferedImage;
import java.io.IOException;
//custom classes
import static utilities.Image_Helper.*;
import utilities.OCV;

public class Octave {

    public int[][][] data; //image data in the Octave at varying levels of blur data[blur][x][y], 0= "no blur"
    public int level; //how many times has the original image been halved
    public int numBlur;
    public int width;
    public int height;

    // For TESTING class octaves
    ////Min numBlur is 3 -> at least 3 needed to compute extrema from DoG
    public static void main(String[] args) throws IOException {
        String fileName = "C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\Images\\test.jpg" ;
        Mat imgMat = OCV.imageToMatrix(fileName);
        int[][][] imgArr = new int[1][imgMat.cols()][imgMat.rows()];
        imgArr[0] = OCV.matrixToArray(imgMat);

        //Octaves
        int numOctaves = 5;
        int numBlur = 1;

        Octave[] octArr = new Octave[numOctaves];
        //initial Octave
        octArr[0] = new Octave(0,imgArr,numBlur);

        BufferedImage img = arrayToImage(octArr[0].data[0]);
        displayImage(img);

        for (int i = 0, a=1; i < numOctaves-1 ; i++) {
            octArr[i+1] = octArr[i].downOctave();
            imgArr[0] = octArr[i+1].data[0];
            img = arrayToImage(imgArr[0]);
            displayImage(img);
        }
    }
    /////////// END OF MAIN ///////////

    public Octave(int inputLevel, int[][][] imgArr,int numBlur) {
        this.width = imgArr.length;
        this.height = imgArr[0].length;
        this.numBlur = numBlur;
        this.level = inputLevel; //how many times has the original image been halved
        this.data = new int[numBlur][width][height];
        this.data = imgArr; //array of images at varying levels of blur data[blur][x][y]
    }

    public Octave downOctave() {
        //Assumption: imgArr is rectangular
        //int[x][y] = int[col][row]

        int[][] imgArr = this.data[0];
        this.width = imgArr.length;
        this.height = imgArr[0].length;
        int[][][] outputImgArr = new int[numBlur][width / 2][height / 2];

        //creating next level of octave with same data as original octave that has been scaled down
        for (int blur = 0; blur < numBlur ; blur++) {
            imgArr = this.data[blur];
            // scaling
            for (int col = 0; col < width / 2; col++) {
                for (int row = 0; row < height / 2; row++) {
                    outputImgArr[blur][col][row] = imgArr[col * 2][row * 2];
                }
            }
        }
        //update new Octave's data
        Octave newOct = new Octave(this.level + 1, outputImgArr,this.numBlur);

     return newOct;
    }

}
