package SIFT;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;

//import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import utilities.Image_Helper;
import utilities.OCV;

import java.awt.image.BufferedImage;

import static javax.swing.text.StyleConstants.Size;

public class Filter {

    //TESTING
    public static void main( String[] args ) {
        try {
            System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
            Mat inputImgMat = OCV.imageToMatrix("C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\Images\\test.jpg");

            Mat outputImgMat = gaussianBlur(inputImgMat, 7,2);
            int [][] imgArr = OCV.matrixToArray(outputImgMat);
            //int[][] imgArr = gaussianBlur(inputImgMat, 7,2);


            BufferedImage img = Image_Helper.arrayToImage(imgArr);
            Image_Helper.displayImage(img);

        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
    }
    /////////// END OF MAIN ///////////


    //Just keep the rule of the radius of the Gaussian Kernel to be 4-5 times the STD.
    //If you set the STD to 4, use Kernel of size [33, 33] to the least.
    //https://stackoverflow.com/questions/28688787/optimal-value-for-applying-gaussian-filter-in-difference-of-gaussian

    public static Mat gaussianBlur (Mat inputMat,  int filterSize, int sigma) {
        //Assumption 1: square filter
        //Assumption 2: dimensions must be odd i.e. not divisible by 2
        if (filterSize %2 == 0){ throw new IllegalArgumentException("filterSize must not be divisible by 2 i.e. even");}

        Mat outputMat = new Mat(inputMat.rows(),inputMat.cols(),inputMat.type());
        Imgproc.GaussianBlur(inputMat, outputMat,new Size(filterSize,filterSize), sigma,sigma,0);

        return outputMat;
    }

    // Differnce of Gaussians => an approximation of the Laplace of the image at a given scale

}



