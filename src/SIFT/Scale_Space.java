package SIFT;

import org.opencv.core.Mat;
import utilities.OCV;
import utilities.TupleExtrema;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static utilities.Image_Helper.*;

public class Scale_Space {

    public Octave[] octArr;
    public int numOctave;
    public int numBlur;

    //TESTING
    public static void main( String[] args ) throws IOException {
       String fileName = "C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\utilities\\test1.jpg" ;
       BufferedImage img;

       //Hyperparameters
       int numOctave = 6; //TODO still only displaying numOctave -1 octaves ??
       int numBlur = 7;

       //Instantiate Scale_Space object
       Scale_Space ss = new Scale_Space(fileName,numOctave,numBlur);
//        Display
//        for (int oct = 0; oct < ss.numOctave-1 ; oct++) { //TODO why does this fix the thing?? changed from -1 to -2
//            for (int blur = 0; blur < ss.numBlur-1 ; blur++) {
//                System.out.println("oct" + oct + "blur" + blur);
//                img = arrayToImage(ss.octArr[oct].data[blur]); //data[blur][x][y] = image
//                displayImage(img);
//            }
//        }

//        //DoG & Extrema
        TupleExtrema allextArr[][] = new TupleExtrema[numOctave][(ss.octArr[0].width*ss.octArr[0].height)/(3*3*3)];
        for (int oct = 0; oct < ss.numOctave-1 ; oct++) {
            int width = ss.octArr[oct].width;
            int height = ss.octArr[oct].height;
            int[][][] DoGArr = DoG.calc(ss.octArr[oct]);
            List<TupleExtrema> extList = new ArrayList<TupleExtrema>();
//            TupleExtrema [] extArr = new TupleExtrema[(height * width * numBlur)]; //what is the theortical max number of extrema ?? //TODO need to make this a list
            System.out.println("Theretical limit of extrema: " + ((height * width * numBlur) / (3 * 3 * 3)));
            int count = 0;
            for (int blur = 0; blur < ss.numBlur-1 ; blur++) {
                //Extrema
                if (blur > 1 && blur<DoGArr.length) {
                    int stride = 5 - oct; //dynamically decrease stride as octave level increases
                    for (int x = 0; x < width; x=x+stride) { //Stride
//                        System.out.println("x: " + x);
                        for (int y = 0; y < height ; y=y+stride) {
//                            System.out.println("y: " + y);
                            if (Extrema.isMax(x,y,DoGArr[blur-1],DoGArr[blur],DoGArr[blur+1])){
                                extList.add(new TupleExtrema(blur,x,y, DoGArr[blur][x][y]));
//                                extArr[count] =  new TupleExtrema(blur,x,y, DoGArr[blur][x][y]); //TODO list.add
                                count ++;
                            }
                        }

                    }
                }
//                Display DoG
//                img = arrayToImage(DoGArr[blur]);
//                displayImage(img);

            }

            TupleExtrema [] extArr = new TupleExtrema[count];
            for (int i = 0; i <count; i++) {
                extArr[i] = extList.get(i);
            }
            allextArr[oct] = extArr;
//
//            //Display Extrema
            //extrema on black
            int[][] arr = new int[width][height];
            //extrema on original base image
            BufferedImage imgBase = getImage ("C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\utilities\\test1.jpg");
            int[][] arrBase = imageToArray(imgBase);
            //extrema on DoG
            int [][] arrDoG = DoGArr[0];
//
            System.out.println("extArr length: " + extArr.length);
            for (int i = 0; i < extArr.length; i++) {
                try {
                    int x =  extArr[i].x;
                    int y = extArr[i].y;
//                    System.out.println("blur: " + extArr[i].blur + " x: " + extArr[i].x + " y: " + extArr[i].y);
                    arr[x][y] = 255;
                    arrBase[x][y] = 255;
                    arrDoG[x][y] = 255;
                } catch (NullPointerException npe) {
                }
//
            }
            img = arrayToImage(arr);
            displayImage(img);

            imgBase = arrayToImage(arrBase);
            displayImage(imgBase);

            BufferedImage imgDoG = arrayToImage(arrDoG);
            displayImage(imgDoG);
        }
    }
    /////////// END OF MAIN ///////////

    public Scale_Space(String fileName,int numOctave, int numBlur) throws IOException {
        //Min numBlur is 3 -> at least 3 needed to compute extrema from DoG
        if (numBlur<3){ throw new IllegalArgumentException("numBlur needs to be larger than 2");}

        //Load image
        Mat imgMat = OCV.imageToMatrix(fileName);
        this.numBlur = numBlur;
        this.numOctave = numOctave; //TODO sort out array out of bound so this isn't effectively 3
        BufferedImage img;

        //some silliness with arrays... pretty much a hack
        int [][] imgArr = OCV.matrixToArray(imgMat);
        int width = imgArr.length;
        int height = imgArr[0].length;
        int [][][] imgArrArr = new int [numBlur][width][height];

        //OCTAVES//
        //Octave array
        this.octArr = new Octave[numOctave]; //octArr is a class variable and carries most the information for the Scale_Space
        //initial Octave
        octArr[0] = new Octave(0,imgArrArr,numBlur);

        //BLUR OCTAVE//
        //Variably blur all images in initial octave
        //Blur Matrix array
        Mat[] imgMatArr = new Mat[numBlur];

        for (int blur = 0; blur < numBlur; blur++) {
            if (blur == 0) {imgMatArr[blur] = imgMat;} //first blur = 0 = no blur
            else {
                imgMatArr[blur] = Filter.gaussianBlur(imgMat, 45, blur*2 ); //amount of blur increased here
            }
            imgArr = OCV.matrixToArray(imgMatArr[blur]);
            octArr[0].data[blur] = imgArr; //at blurLevel blur save image to octave data
        }

        //DOWN OCTAVE with BLUR//
        //(imagine a deck of solitaire cards left is least blurry right is most blurry and down are scaled down octaves
        //next levels of octaves
        for (int oct = 0; oct < numOctave-1 ; oct++) {
            octArr[oct+1] = octArr[oct].downOctave(); //returns new smaller octave //TODO workout why array is going out of bounds
        }
    }
}
