package utilities;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Image_Helper {

    public static void main(String[] args) throws IOException {
        //TODO investigate why image is getting washed out when converted to and from and array
        // https://gyazo.com/20f3d1e7fcd11f085f39230d6ae2f05a

        //used to TEST image helper methods

        //TODO determine why file name has to be absolute path and not relative path
        String fileName = "C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\utilities\\test.jpg";
        BufferedImage img = getImage(fileName);
        img = convertToGrayScale(img);
        displayImage(img);

        System.out.println("Width " + img.getWidth());
        System.out.println("Height " + img.getHeight());

        int[][] twoDImgArr = imageToArray(img);

        //TEST 2D Image Array
//        System.out.println("\nImage array:" );
//        for (int i = 0; i < img.getWidth(); i++) { // [x]
//            System.out.println("");
//            for (int j = 0; j < img.getHeight(); j++) { //[y]
//                System.out.print(imgArr2D[i][j] + " "); //[x][y] coordinates
//            }
//        }

        //TEST 1D Image Array
        System.out.println("Image Array 1D: ");
        int[] oneDImgArr = twoDArray_To_oneDArray(twoDImgArr);
        for (int a = 0; a < oneDImgArr.length; a++) System.out.print(oneDImgArr[a] + " "); //print array

        //TEST converting the int array to an image
        img = arrayToImage(twoDImgArr);
        displayImage(img);

    }
    /////////// END OF MAIN ///////////

    //https://stackoverflow.com/questions/7225957/java-awt-image-from-file
    public static BufferedImage getImage(String fileName) {
        try {
            File pathToFile = new File(fileName);
            BufferedImage img = ImageIO.read(pathToFile); //why to use buffered image -> https://stackoverflow.com/questions/3944825/difference-between-the-image-and-bufferedimage-in-java/3945274
            return img;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static void displayImage(BufferedImage img) throws IOException {
        int width = img.getWidth();
        int height = img.getHeight();
        ImageIcon icon = new ImageIcon(img);
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());
        frame.setSize(width, height);
        JLabel lbl = new JLabel();
        lbl.setIcon(icon);
        frame.add(lbl);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    //TODO possibly better performance using image vs buffered image? //https://stackoverflow.com/questions/9131678/convert-a-rgb-image-to-grayscale-image-reducing-the-memory-in-java
    //FIXME seems this convertToGrayScale is washing out the images a bit
    public static BufferedImage convertToGrayScale(BufferedImage img_Color) throws IOException {
        int width = img_Color.getWidth();
        int height = img_Color.getHeight();
        BufferedImage img_Gray = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
        Graphics g_Gray = img_Gray.getGraphics();
        g_Gray.drawImage(img_Color, 0, 0, null);
        g_Gray.dispose();

        return img_Gray;
    }


    public static int[][] imageToArray(BufferedImage img) throws IOException {
        int width = img.getWidth();
        int height = img.getHeight();
        int[][] imgArr = new int[width][height];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                imgArr[i][j] = img.getRGB(i, j) & 0xFF; // uses x,y coordinates => i = x = width
            }
        }
        return imgArr;
    }

    public static int[] twoDArray_To_oneDArray(int twoDArr[][]) {
        int width = twoDArr.length;
        int height = twoDArr[0].length;

        int[] oneDArr = new int[(width * height)];
        for (int col = 0; col < width; col++) {
            for (int row = 0; row < height; row++) {
                oneDArr[width * row + col] = twoDArr[col][row];
            }
        }
        return oneDArr;
    }

    public static int[][] oneDArray_To_twoDArray(int oneDArr[], int width, int height) {
        int[][] twoDArr = new int[width][height]; //[x][y]
        for (int col = 0; col < width; col++) {
            for (int row = 0; row < height; row++) {
                twoDArr[col][row] = oneDArr[width * row + col];
            }
        }
        return twoDArr;
    }

    public static BufferedImage arrayToImage(int[][] imgArr) {
        //Both options below work
        //TODO determine which is faster
        //TODO IF OPTION_1 faster THEN rather use 2D int array
        int width = imgArr.length;
        int height = imgArr[0].length;

        int[] imgArr1D = twoDArray_To_oneDArray(imgArr);
        //OPTION_1
        BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster raster = outputImage.getRaster();
        for (int row = 0, nb = 0; row < height; row++) {
            for (int col = 0; col < width; col++, nb++) {
                raster.setSample(col, row, 0, imgArr1D[nb]);
            }
        }
        return outputImage;
    }

    public static int[][] extremaToArr(TupleExtrema[] extremaArr, int width, int height) {
        int[][] outputArr = new int[width][height]; //[x][y]

        for (int i = 0; i < extremaArr.length; i++) {
            try {
                int x = extremaArr[i].x;
                int y = extremaArr[i].y;
                outputArr[x][y] = 255;
            } catch (NullPointerException npe) {}
        }
        return outputArr;
    }

    public static void displayAndSaveImageWithExtrema(String fileName, TupleExtrema[] extremaArr, int testNum, String src_dst) throws IOException {
        BufferedImage imgBase = getImage (fileName);
        int[][] arrBase = imageToArray(imgBase);
        for (int i = 0; i < extremaArr.length; i++) {
            try {
                int x = extremaArr[i].x;
                int y = extremaArr[i].y;
                arrBase[x][y] = 255;
            } catch (NullPointerException npe) {
            }
        }
        imgBase = arrayToImage(arrBase);
        displayImage(imgBase);
        FileOutputStream out = new FileOutputStream("C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\Results\\Test"+testNum+"\\"+src_dst+"_Extrema.png");
        ImageIO.write(imgBase, "png", out);
    }

}





