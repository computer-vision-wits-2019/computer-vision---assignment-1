package utilities;

import org.opencv.core.Point;

public class TupleExtrema {
        public final int blur;
        public final int x;
        public final int y;
        public int contrastLevel;
        public final int keyPointScore; //gets incremented if overlaps with blur from various contiguous levels
        public Point match;

        public TupleExtrema(int blur, int x, int y, int contrastLevel) {
            this.blur = blur;
            this.x = x;
            this.y = y;
            this.contrastLevel = contrastLevel;
            this.keyPointScore = 0;
            this.match = new Point(0,0);
        }

    public int getContrastLevel() {
        return this.contrastLevel;
    }
}
