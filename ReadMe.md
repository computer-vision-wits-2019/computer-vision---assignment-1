# Computer Vision Assignment 1 - SIFT and Image Stitching 

## Folder Structure Overview

- driver -> code -> main.java [This is the central code to run for this implementation]
- Images -> Test images 1-3
- Results -> Results from Tests, include Scale-Space Image pyramids and Extrema points 
- SIFT -> Classes relating specifically to solving various stages of the SIFT pipeline 
- utilities -> helper classes for images, OpenCV (OCV) interfacing, Linear Algebra 

## Before running...

The relative file paths did not seem to be working, so the file paths present only relate to my local machine. One needs to take the truncated file path e.g. \\CV_Assignment_1\\src\\Images\\test"+testNum+",1.jpg" OR \\CV_Assignment_1\\src\\Results\\Test"+testNum+"\\SRC_Octave"+oct+"_Blur"+blur+".png" for image loading / image saving and add the appropriate root / upper folders to the file path. 

## Notes on outputs 

- Due to time constraints not all extremas are shown on their base images

- Extremas are collated from multiple DoGs, which is why they sometimes coalesce - taken from Just 3 DoGs one would expect there to be at least always a pixel separation between extrema  

- The findHomography function is producing pretty wild transformation matrices 

  - There are two immediately reasons for this - 1) the matrix values are being rounded to integers (even though their data type is double) 2) less than half of the input points are properly matched, therefore RANSAC will would not converge 

- To mitigate the integer rounding effects on the outputs a scalar of 0.05 has been applied to the first two columns of the transformation matrix 

- When this scalar is set to 0.0, the translation column, i.e. the third column of the transformation matrix still does not produce reliable image stitching on image Test1 which is translated and only mildly scaled -> this means it is likely that the input points are a likely culprit. 

  

  ## Notes on Matching and Descriptors 

  -  Only matching by similar / the same contrast was implemented -> points were then picked at random from match lists between 10-100 long
  - To still be implemented are further descriptors:
    - KeyPoint score that is incremented according to the presence of the same extrema at multiple octaves and scales 
    - Gradient direction vector descriptors for more robust rotational invariance 

# Results

To save space for the upload, only Test 1 results were retained. Test 2 and 3 will be generated and stored automatically when the program is run. 

# Enjoy =) E.L.E. 





